#!/usr/bin/env python3

import os
import sys
import csv
import codecs
import datetime
import argparse
import traceback
import unicodedata
import googleapiclient.discovery

API_SERVICE_NAME = "youtube"
API_VERSION = "v3"
COMMENT_KIND = "youtube#comment"

OUT_FILE_FMT = "%04d_%s_%s_n%d_%s.txt"
JSON_DATE_FMT = "%Y-%m-%dT%H:%M:%SZ"
DATE_FMT = "%d%m%Y"
CSV_DATE_FMT = "%d/%m/%Y"

class VideoItem:
    def __init__(self, id:int, youtubeId:str, maxDate:datetime.datetime):
        self.id = id
        self.youtubeId = youtubeId
        self.maxDate = maxDate

        self.maxDate += datetime.timedelta(days=1.0)

    def __repr__(self):
        return f"({self.id}, {self.youtubeId}, {self.maxDate})"

class PagedGoogleApiRequest:
    def __init__(self, requestCreator):
        self._createRequest = requestCreator
        self._response:dict
        self._pageToken:str

        self._response = None
        self._pageToken = None

    def response(self):
        return self._response

    def move_next(self):
        if not self._response:
            return True

        self._pageToken = None
        if "nextPageToken" in self._response:
            self._pageToken = self._response["nextPageToken"]

        return self._pageToken != None

    def execute(self):
        self._response = execute_googleapi_request(
            self._createRequest(self._pageToken)
        )

        return self._response is not None

class UserDatabase:
    db = dict()
    count = 0

    @classmethod
    def anon(c, userName:str):
        userId = c.db.get(userName)

        if userId is not None:
            return userId

        userId = "U%05d" % (hash(userName) % 99999)
        c.db[userName] = userId
        c.count+=1

        return userId

def download_comments_for_video(youtube:object, videoId:int, youtubeId:str, maxDate:datetime.date):
    # Disable OAuthlib's HTTPS verification when running locally.
    # *DO NOT* leave this option enabled in production.
    # os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    existsRequest = youtube.videos().list(part="snippet", id=youtubeId)
    existsResponse = execute_googleapi_request(existsRequest)
    if existsResponse is None or existsResponse["pageInfo"]["totalResults"] == 0:
        raise Exception("Error: video not found (%s)" % youtubeId)

    textOut = open("temp_text", "w", encoding="utf-8")
    metaOut = open("temp_meta", "w", encoding="utf-8")
    total = 0

    threadsRequest = PagedGoogleApiRequest(
        lambda pageToken:
            youtube.commentThreads().list(
                order = "time", # "relevance",
                part = "snippet",
                pageToken = pageToken,
                videoId = youtubeId,
                textFormat = "plainText"
            )
    )

    videoAuthor = existsResponse["items"][0]["snippet"]["channelTitle"]
    UserDatabase.anon(videoAuthor)

    while threadsRequest.move_next():
        if not threadsRequest.execute():
            break

        response = threadsRequest.response()

        for thread in response["items"]:
            parentComment = thread["snippet"]["topLevelComment"]
            commentDate = datetime.datetime.strptime(parentComment["snippet"]["publishedAt"], JSON_DATE_FMT)

            if commentDate > maxDate:
                continue

            if "kind" not in parentComment or parentComment["kind"] != COMMENT_KIND:
                continue

            total += 1

            authorName = parentComment["snippet"]["authorDisplayName"]
            threadUsers = { authorName, videoAuthor }
            userId = UserDatabase.anon(authorName)
            text = prepare_comment_text(parentComment["snippet"]["textDisplay"], threadUsers)

            textOut.write(format_comment_textual(userId, text))
            metaOut.write(format_comment_meta("C", userId, text, parentComment))

            if thread["snippet"]["totalReplyCount"] > 0:
                repliesRequest = PagedGoogleApiRequest(
                    lambda pageToken:
                        youtube.comments().list(
                            part = "snippet",
                            pageToken = pageToken,
                            parentId = parentComment["id"]
                        )
                )

                while repliesRequest.move_next():
                    if not repliesRequest.execute():
                        break

                    repliesResponse = repliesRequest.response()

                    repliesResponse["items"].sort(key=get_comment_publish_date)

                    for reply in repliesResponse["items"]:
                        replyDate = datetime.datetime.strptime(reply["snippet"]["publishedAt"], JSON_DATE_FMT)

                        if replyDate > maxDate:
                            continue

                        total += 1

                        authorName = reply["snippet"]["authorDisplayName"]
                        threadUsers.add(authorName)
                        userId = UserDatabase.anon(authorName)
                        text = prepare_comment_text(reply["snippet"]["textDisplay"], threadUsers)

                        textOut.write("    %s" % format_comment_textual(userId, text))
                        metaOut.write(format_comment_meta("R", userId, text, reply))

    textOut.close()
    metaOut.close()

    publishDate = datetime.datetime.strptime(existsResponse["items"][0]["snippet"]["publishedAt"], JSON_DATE_FMT)
    publishDate = publishDate.strftime(DATE_FMT)
    today = datetime.date.today().strftime(DATE_FMT)

    os.replace(textOut.name, OUT_FILE_FMT % (videoId, publishDate, today, total, "textual"))
    os.replace(metaOut.name, OUT_FILE_FMT % (videoId, publishDate, today, total, "meta"))

def execute_googleapi_request(request):
    try:
        return request.execute()
    except googleapiclient.errors.HttpError as http:
        raise Exception().with_traceback(http)
    except googleapiclient.errors.Error as error:
        raise Exception().with_traceback(error)

def get_comment_publish_date(commentObj):
    if "kind" not in commentObj or commentObj["kind"] != COMMENT_KIND:
        return None

    return commentObj["snippet"]["publishedAt"]

def prepare_comment_text(text, users:set):
    for u in users:
        text = text.replace(u, UserDatabase.anon(u))

    text = text.replace('\n', ' ')
    return unicodedata.normalize("NFKD", text)

def format_comment_textual(userId:str, text:str):
    return "%s said: %s\n" % (
        userId,
        text
    )

def format_comment_meta(commentType:str, userId:str, text:str, commentObj):
    snippet = commentObj["snippet"]
    return "%s,%s,%u,%s,%s\n" % (
        commentType,
        datetime.datetime.strptime(snippet["publishedAt"], JSON_DATE_FMT).strftime(CSV_DATE_FMT),
        snippet["likeCount"],
        userId,
        text
    )

def get_file_start_without_bom(filePath:str):
    fileStart = 0
    with open(filePath, "rb") as file:
        bomTest = file.read(len(codecs.BOM_UTF8))
        if bomTest.startswith(codecs.BOM_UTF8):
            fileStart = len(codecs.BOM_UTF8)

    return fileStart

if __name__ == "__main__":
    exit_code = 0
    parser = argparse.ArgumentParser(description="YouTube comment downloader")
    parser.add_argument("--input", required=True, type=str, help="File containing CSV list of tuples (videoId[integer], youtubeId[string], maxDate[dd/mm/YY])")

    args = parser.parse_args()

    with open("developer.key", "rb") as devKeyFile:
        devKey = devKeyFile.readline()

    youtube = googleapiclient.discovery.build(
        API_SERVICE_NAME,
        API_VERSION,
        developerKey = devKey
    )

    fileStart = get_file_start_without_bom(args.input)
    inputFile = open(args.input, "r")

    inputFile.seek(fileStart, 0)
    dialect = csv.Sniffer().sniff(inputFile.readline())

    inputFile.seek(fileStart, 0)
    inputReader = csv.reader(inputFile, dialect=dialect)

    videos = []
    currentLine = 0
    try:
        for row in inputReader:
            currentLine += 1
            videos.append(VideoItem(id=int(row[0]), youtubeId=row[1], maxDate=datetime.datetime.strptime(row[2], CSV_DATE_FMT)))

    except Exception as err:
        print(f"Error while processing input csv file at line {currentLine}:")
        print(err)
        traceback.print_tb(err.__traceback__)
        exit_code = 1

    finally:
        inputFile.close()
        if exit_code:
            exit(exit_code)

    try:
        for item in videos:
            sys.stdout.write("Processing video %d..." % item.id)
            sys.stdout.flush()

            download_comments_for_video(youtube, item.id, item.youtubeId, item.maxDate)

            sys.stdout.write("done\r\n")
            sys.stdout.flush()

    except Exception as err:
        sys.stdout.write("\r\n")
        sys.stdout.flush()
        print("Processing stopped due to an error:", flush=True)
        print(err)
        traceback.print_tb(err.__traceback__)
        exit_code = 1

    exit(exit_code)
